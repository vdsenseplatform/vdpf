install-dev:
	pip3 install --upgrade -e .[dev,all]

install:
	pip3 install --upgrade .[all]

COMPOSE=docker-compose -f docker-compose.int.yml

start-ext: stop-ext
	$(COMPOSE) up -d

stop-ext:
	$(COMPOSE) down

test-integration:
	pytest tests/integration

test-unit:
	pytest tests/unit

test:
	pytest --cov=vdpf tests/
