import aiounittest
import unittest

from vdpf.elastic import Elasticsearch, AIOElasticSearch


class TestElasticSearch(unittest.TestCase):
    def setUp(self):
        self.conf_path = './tests/configs/elastic.json'

    def test_client_connection(self):
        client = Elasticsearch.from_config_file(fp=self.conf_path)

        doc = {
            'author': 'test',
            'text': 'test'
        }
        client.index(index='test', doc_type='doc', id=1, body=doc)
        res = client.get(index='test', doc_type='doc', id=1)

        self.assertTrue(res['found'])


class TestElasticSearchAsync(aiounittest.AsyncTestCase):
    def setUp(self):
        self.conf_path = './tests/configs/elastic.json'

    async def test_client_connection(self):
        client = AIOElasticSearch.from_config_file(fp=self.conf_path)

        doc = {
            'author': 'test',
            'text': 'test'
        }
        client.index(index='test', doc_type='doc', id=1, body=doc)
        res = await client.get(index='test', doc_type='doc', id=1)

        self.assertTrue(res['found'])
