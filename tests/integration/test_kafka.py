import unittest
import aiounittest
import asyncio

from vdpf.kafka_ import KafkaProducer, AIOKafkaProducer,\
    KafkaConsumer, AIOKafkaConsumer, safe_send


class TestKafka(unittest.TestCase):
    def setUp(self):
        self.producer_path = './tests/configs/kafka.producer.json'
        self.consumer_path = './tests/configs/kafka.consumer.json'
    
    def test_producer_send(self):
        producer = KafkaProducer.from_config_file(fp=self.producer_path)
        producer.send('test', {'foo': 'bar'}).get()

    def test_consumer_recv(self):
        producer = KafkaProducer.from_config_file(fp=self.producer_path)
        producer.send('test', {'foo': 'bar'}).get()

        consumer = KafkaConsumer.from_config_file('test', fp=self.consumer_path,
            auto_offset_reset='earliest')
        msg = next(consumer).value
        self.assertDictEqual(msg, {'foo': 'bar'})

    def test_producer_safe_send(self):
        producer = KafkaProducer.from_config_file(fp=self.producer_path)
        with safe_send():
            producer.send('test', {'foo': 'bar'}).get(timeout=0.001)


class TestKafkaAsync(aiounittest.AsyncTestCase):
    def setUp(self):
        self.producer_path = './tests/configs/kafka.producer.json'
        self.consumer_path = './tests/configs/kafka.consumer.json'
    
    async def test_producer_send(self):
        producer = AIOKafkaProducer.from_config_file(fp=self.producer_path)
        
        await producer.start()
        await producer.send('test', {'foo': 'bar'})
        await producer.stop()

    async def test_consumer_recv(self):
        producer = AIOKafkaProducer.from_config_file(fp=self.producer_path)
        
        await producer.start()
        await producer.send('test', {'foo': 'bar'})
        await producer.stop()

        consumer = AIOKafkaConsumer.from_config_file('test', fp=self.consumer_path,
            auto_offset_reset='earliest')
        await consumer.start()
        msg = await consumer.getone()
        msg = msg.value
        self.assertDictEqual(msg, {'foo': 'bar'})
        await consumer.stop()

