import unittest
import aiounittest

from vdpf.storage import StorageServiceClient


class TestStorage(unittest.TestCase):
    def setUp(self):
        # StorageServiceClient.SERVICE_URL = 'http://localhost:3000/v1'
        self.client = StorageServiceClient.from_config_file('ekyc')

    def test_upload(self):
        folder = 'test'
        fname = 'foo.txt'
        content = b'123456'

        url = self.client.upload_file(content, folder,
            filename=fname)
        self.assertEqual(url, 'https://storage.vdsense.com/v1/ekyc/test/foo.txt')

    def test_upload_anon(self):
        folder = 'test'
        content = b'123456'

        url = self.client.upload_file(content, folder)
        self.assertTrue(url.startswith('https://storage.vdsense.com/v1/ekyc/test'))

    def test_upload_fut(self):
        folder = 'test'
        content = b'123456'

        fut = self.client.upload_file_future(content, folder)
        url = fut.result()
        self.assertTrue(url.startswith('https://storage.vdsense.com/v1/ekyc/test'))

    def test_get(self):
        url = 'https://storage.vdsense.com/v1/ekyc/test/foo.txt'

        bytes_ = self.client.get_file(url)
        self.assertEqual(bytes_, b'123456')


class TestStorageAsync(aiounittest.AsyncTestCase):
    def setUp(self):
        # StorageServiceClient.SERVICE_URL = 'http://localhost:3000/v1'
        self.client = StorageServiceClient.from_config_file('ekyc')

    async def test_upload(self):
        folder = 'test'
        fname = 'foo.txt'
        content = b'123456'

        url = await self.client.upload_file_async(content, folder,
            filename=fname)
        self.assertEqual(url, 'https://storage.vdsense.com/v1/ekyc/test/foo.txt')

    async def test_get(self):
        url = 'https://storage.vdsense.com/v1/ekyc/test/foo.txt'

        bytes_ = await self.client.get_file_async(url)
        self.assertEqual(bytes_, b'123456')
