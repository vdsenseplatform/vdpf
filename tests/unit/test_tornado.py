import json


from tornado.web import Application
from tornado.testing import AsyncHTTPTestCase, gen_test

from vdpf.tornado_ import BaseHttpHandler,\
    BaseWebSocketHandler, middlewares
from vdpf.mml import Schema, fields


class EchoHandler(BaseHttpHandler):
    @middlewares.json_payload
    def post(self, payload):
        self.set_status(200)
        self.write(payload)
        self.finish()


class Foo:
    def __init__(self, b1, b2):
        self.b1 = b1
        self.b2 = b2


class FooSchema(Schema):
    _cls = Foo
    b1 = fields.String()
    b2 = fields.Integer()


class SchemaHandler(BaseHttpHandler):
    @middlewares.json_schema(FooSchema)
    def post(self, req):
        self.set_status(200)
        self.finish()


class TestSampleApp(AsyncHTTPTestCase):
    def get_app(self):
        return Application([
            (r'/echo', EchoHandler),
            (r'/schema', SchemaHandler)
        ])
    
    def test_echo(self):
        payload = {
            'foo': 'bar',
            'foo1': [
                'bar1', 'bar2'
            ]
        }
        response = self.fetch(
            '/echo', method='POST',
            raise_error=True, body=json.dumps(payload)
        )
        res = json.loads(response.body.decode())

        self.assertDictEqual(payload, res)

    def test_bad_echo(self):
        payload = '{x,'
        response = self.fetch(
            '/echo', method='POST',
            raise_error=False, body=payload
        )
        code = response.code
        self.assertEqual(code, 400)

    def test_schema(self):
        payload = {
            'b1': 'XA',
            'b2': 1
        }
        response = self.fetch(
            '/schema', method='POST',
            raise_error=True, body=json.dumps(payload)
        )

    def test_bad_schema(self):
        payload = {
            'b1': 22,
            'b2': '1'
        }
        response = self.fetch(
            '/schema', method='POST',
            raise_error=False, body=json.dumps(payload)
        )
        self.assertEqual(response.code, 400)

