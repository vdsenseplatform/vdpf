import unittest

from vdpf import utils
from vdpf.log import monkeypatch


class TestIOUtils(unittest.TestCase):
    def test_yaml_configs(self):
        conf1 = utils.config_from_file('tests/configs/sample.yml')
        conf = {
            'foo1': {
                'bar1': 1, 'bar2': True
            },
            'foo2': {
                'bar3': [1, 2, 3]
            }
        }
        self.assertDictEqual(conf1, conf)


class TestLog(unittest.TestCase):
    def test_monkeypatch(self):
        monkeypatch()
        print('Test')
        print(1)
        print({'a': 'b'})
