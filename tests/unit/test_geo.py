import unittest

from vdpf.geo import Point, Polygon, SPoint, SPolygon
from vdpf.mml import geo


class TestGeo(unittest.TestCase):
    def test_point_load(self):
        d = {
            'x': 1.5,
            'y': 2.5
        }

        o = geo.PointSchema().load(d).data
        self.assertIsInstance(o, Point)

    def test_point_dump(self):
        o = Point(1.5, 2.5)
        d = geo.PointSchema().dump(o).data

        self.assertEqual(d['x'], 1.5)
        self.assertEqual(d['y'], 2.5)

    def test_point_to_shapely(self):
        o = Point(1.5, 2.5)
        so = o.to_shapely()
        self.assertListEqual(list(so.bounds), [1.5, 2.5, 1.5, 2.5])

    def test_polygon_load(self):
        d = {
            'vertices': [
                {'x': 1, 'y': 1},
                {'x': 2, 'y': 1},
                {'x': 2, 'y': 2},
                {'x': 1, 'y': 2}
            ]
        }

        o = geo.PolygonSchema().load(d).data
        self.assertIsInstance(o, Polygon)

    def test_polygon_dump(self):
        o = Polygon([
            Point(1, 1), Point(2, 1),
            Point(2, 2), Point(1, 2)
        ])
        d = geo.PolygonSchema().dump(o).data

    def test_polygon_to_shapely(self):
        o = Polygon([
            Point(1, 1), Point(2, 1),
            Point(2, 2), Point(1, 2)
        ])
        so = o.to_shapely()
        self.assertEqual(so.area, 1)
