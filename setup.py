import os

from setuptools import setup, find_packages

kafka_reqs = [
    'kafka-python<2', 'aiokafka<1',
    'crc32c'
]
tornado_reqs = [
    'tornado'
]
mml_reqs = [
    'marshmallow==2.18.1'
]
elastic_reqs = [
    'elasticsearch>=7.0.0',
    'aioelasticsearch'
]
geo_reqs = [
    'shapely==1.7.0'
]

setup(
    name='vdpf',
    description='VDSense platform library',
    version='0.5.1',
    url='',
    author='lanpn',
    author_email='lanpn@vdsense.com',
    classifiers=[
        'Programming Language :: Python :: 3.5'
    ],
    package_dir={'vdpf': 'vdpf'},
    packages=find_packages(exclude='tests'),
    python_requires='>=3.5, <4',
    install_requires=[
        'numpy', 'loguru', 'pybase64',
        'pyyaml', 'requests', 'typing',
        'aiohttp[speedups]'
    ],
    extras_require={
        'dev': ['pytest', 'pytest-cov', 'aiounittest'],
        'kafka': kafka_reqs,
        'tornado': tornado_reqs,
        'mml': mml_reqs,
        'elastic': elastic_reqs,
        'geo': geo_reqs,
        'all': kafka_reqs + tornado_reqs
            + mml_reqs + elastic_reqs + geo_reqs
    }
)
