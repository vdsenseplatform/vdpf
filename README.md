# VDSense Platform Library
Python platform utilities for VDSense AI products

This library currently includes:

- Configuration helpers for [Elasticsearch](https://elasticsearch-py.readthedocs.io/en/master/index.html) (`vdpf.elastic`), [Kafka](https://kafka-python.readthedocs.io/en/master/index.html) and [AIOKafka](https://aiokafka.readthedocs.io/en/stable/index.html) (`vdpf.kafka_`)
- Base classes for the [Tornado](https://www.tornadoweb.org/en/stable/) web framework (`vdpf.tornado_`)
- Base classes for the Marshmallow validation framework
- Logging utilities based on [loguru](https://github.com/Delgan/loguru) (`vdpf.logs`)

Configuration can be pulled from the vdpf-configs repo into the default location (`/opt/vdsense`)

## Requirements
- Python 3.5 or above

## Quickstart
```bash
# Pull configuration into default location
mkdir -p /opt/vdsense
git clone https://bitbucket.org/vdsenseplatform/vdpf-configs.git /opt/vdsense/vdpf-configs

# Install vdpf
git clone https://bitbucket.org/vdsenseplatform/vdpf.git
cd vdpf
make install
```
