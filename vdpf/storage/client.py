from loguru import logger
from concurrent.futures import ThreadPoolExecutor
from urllib.parse import urljoin

import requests
import aiohttp

from vdpf.utils import config_from_file, as_fp


def future_wrapped(fn):
    def decorate(*args, **kwargs):
        return ThreadPoolExecutor(max_workers=1)\
            .submit(fn, *args, **kwargs)
    return decorate


class StorageServiceClient:
    SERVICE_URL = 'https://storage.vdsense.com/v1'

    @classmethod
    def from_config_file(cls, namespace,
        fp='/opt/vdsense/vdpf-configs/storage.json', **kwargs):
        conf = config_from_file(fp)
        conf.update(kwargs)
        return cls(namespace, **conf)

    def __init__(self, namespace, apikey):
        self.namespace = namespace
        self.apikey = apikey

    async def upload_file_async(self, fp, folder, filename=None,
        namespace=None, return_url_only=True):
        if namespace is None:
            namespace = self.namespace

        path = folder
        if filename is not None:
            path += '/' + filename

        url = '%s/%s/%s' % (self.SERVICE_URL, namespace, path)

        with as_fp(fp) as f:
            files = {
                'file': f
            }

            async with aiohttp.ClientSession() as client:
                async with client.post(url, data=files,
                    headers={
                    'X-Access-Key': self.apikey
                }) as resp:
                    resp.raise_for_status()
                    out = await resp.json()

        if return_url_only:
            return out['url']
        else:
            return out

    def upload_file(self, fp, folder, filename=None, namespace=None,
        return_url_only=True):
        if namespace is None:
            namespace = self.namespace

        path = folder
        if filename is not None:
            path += '/' + filename

        url = '%s/%s/%s' % (self.SERVICE_URL, namespace, path)

        with as_fp(fp) as f:
            files = {
                'file': f
            }
            response = requests.post(url, files=files,
                headers={
                    'X-Access-Key': self.apikey
                }
            )
        response.raise_for_status()
        out = response.json()

        if return_url_only:
            return out['url']
        else:
            return out

    @future_wrapped
    def upload_file_future(self, fp, folder, filename=None,
        namespace=None, return_url_only=True):
        return self.upload_file(fp, folder, filename=filename,
            namespace=namespace, return_url_only=return_url_only)

    def get_file(self, url: str) -> bytes:
        if not url.startswith(self.SERVICE_URL):
            url = urljoin(self.SERVICE_URL, url)

        response = requests.get(url, headers={
            'X-Access-Key': self.apikey
        })
        response.raise_for_status()
        out = response.content
        return out

    @future_wrapped
    def get_file_future(self, url):
        return self.get_file(url)

    async def get_file_async(self, url: str):
        if not url.startswith(self.SERVICE_URL):
            url = urljoin(self.SERVICE_URL, url)

        async with aiohttp.ClientSession() as client:
            async with client.get(url, headers={
                'X-Access-Key': self.apikey
            }) as resp:
                resp.raise_for_status()
                out = await resp.read()

        return out
