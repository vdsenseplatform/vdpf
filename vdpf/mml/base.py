from marshmallow import Schema as OriginalSchema,\
    fields, post_load


class Schema(OriginalSchema):
    _cls = None

    def __init__(self, *args, strict=True, **kwargs):
        super().__init__(*args, strict=strict, **kwargs)

    @post_load
    def make_obj(self, data):
        return self._cls(**data)
