from vdpf.geo import Point, Polygon
from marshmallow import fields, post_load

from .base import Schema


class PointSchema(Schema):
    _cls = Point  # type: ignore

    x = fields.Float()
    y = fields.Float()


class PolygonSchema(Schema):
    _cls = Polygon  # type: ignore

    vertices = fields.List(
        fields.Nested(PointSchema()),
        missing=None, default=None
    )
