import builtins
import loguru
import json
import sys

from contextlib import contextmanager
from loguru import logger


def __print_override(*args, **kwargs):        
    for s in args:
        if isinstance(s, list):
            s = '\n' + str(s)
        if isinstance(s, dict):
            try:
                s = '\n' + json.dumps(s, indent=2)
            except json.JSONDecodeError:
                s = str(s)
        else:
            s = str(s)
        logger.debug(s)


def monkeypatch():
    logger.remove()
    logger.add(sys.stderr, colorize=True, 
        format='[<green>{time}</green>][<level>{level}</level>] <level>{message}</level>')
    builtins.print = __print_override


# @contextmanager
# def catch_exc():
#     try:
