import json
import asyncio

from contextlib import contextmanager
from loguru import logger
from kafka import KafkaProducer as OriginalProducer
from aiokafka import AIOKafkaProducer as OriginalAioProducer
from kafka.errors import KafkaTimeoutError

from vdpf.utils import config_from_file


@contextmanager
def safe_send():
    try:
        yield
    except KafkaTimeoutError:
        logger.warning('Kafka timed out. Message may not be sent')
    except Exception as e:
        logger.exception('Error')


class KafkaProducer(OriginalProducer):
    @classmethod
    def from_config_file(cls, fp='/opt/vdsense/vdpf-configs/kafka.producer.json',
        value_serializer=lambda x: json.dumps(x).encode(), **kwargs):
        conf = config_from_file(fp)
        conf.update(kwargs)
        if 'api_version' in conf.keys():
            vals = conf['api_version'].split('.')
            vals = [int(x) for x in vals]
            conf['api_version'] = tuple(vals)
        return cls(value_serializer=value_serializer, **conf)


class AIOKafkaProducer(OriginalAioProducer):
    @classmethod
    def from_config_file(cls, fp='/opt/vdsense/vdpf-configs/kafka.producer.json',
        value_serializer=lambda x: json.dumps(x).encode(),
        loop=None, **kwargs):
        if loop is None:
            loop = asyncio.get_event_loop()

        conf = config_from_file(fp)
        conf.update(kwargs)

        # Patch args for compatibility
        conf['max_batch_size'] = conf.pop('batch_size', 16384)

        return cls(value_serializer=value_serializer, loop=loop, **conf)
