import json
import asyncio

from contextlib import contextmanager
from loguru import logger
from kafka import KafkaConsumer as OriginalConsumer
from aiokafka import AIOKafkaConsumer as OriginalAioConsumer

from vdpf.utils import config_from_file


class KafkaConsumer(OriginalConsumer):
    @classmethod
    def from_config_file(cls, *topics, fp='/opt/vdsense/vdpf-configs/kafka.consumer.json',
        value_deserializer=lambda x: json.loads(x.decode()), **kwargs):
        conf = config_from_file(fp)
        conf.update(kwargs)

        # Patch args for compatibility
        if 'api_version' in conf.keys():
            vals = conf['api_version'].split('.')
            vals = [int(x) for x in vals]
            conf['api_version'] = tuple(vals)
        return cls(*topics, value_deserializer=value_deserializer, **conf)


class AIOKafkaConsumer(OriginalAioConsumer):
    @classmethod
    def from_config_file(cls, *topics, fp='/opt/vdsense/vdpf-configs/kafka.consumer.json',
        value_deserializer=lambda x: json.loads(x.decode()),
        loop=None, **kwargs):
        if loop is None:
            loop = asyncio.get_event_loop()

        conf = config_from_file(fp)
        conf.update(kwargs)
        # Patch args for compatibility

        return cls(*topics, value_deserializer=value_deserializer, loop=loop, **conf)
