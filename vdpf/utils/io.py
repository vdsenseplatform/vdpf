import os
import io
import json
import yaml

from contextlib import contextmanager
from loguru import logger


@contextmanager
def as_fp(fp, mode='rt'):
    if isinstance(fp, str):
        with open(fp, mode=mode) as f:
            yield f
    elif isinstance(fp, bytes):
        with io.BytesIO(fp) as f:
            yield f
    else:
        yield fp


def config_from_file(fp, ext=None):
    with as_fp(fp) as f:
        if ext == 'json':
            return json.load(f)
        elif ext == 'yaml':
            c = yaml.full_load(f)
            if c is None:
                raise ValueError()
            return c
        else:
            for e in ['json', 'yaml']:
                try:
                    return config_from_file(fp, ext=e)
                except:
                    pass
            raise ValueError('Cannot parse config file')
