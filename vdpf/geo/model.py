from typing import List
from shapely.geometry import Point as SPoint,\
    Polygon as SPolygon


class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def to_shapely(self) -> SPoint:
        return SPoint(self.x, self.y)

    def __hash__(self):
        return hash((self.x, self.y))


class Polygon:
    def __init__(self, vertices: List[Point]):
        self.vertices = vertices

    def __hash__(self):
        return hash(tuple(self.vertices))

    def to_shapely(self) -> SPolygon:
        return SPolygon([
            [p.x, p.y]
            for p in self.vertices
        ])
