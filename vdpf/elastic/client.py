from elasticsearch import Elasticsearch as OriginalElasticsearch
from aioelasticsearch import Elasticsearch as OriginalAIOElasticsearch

from vdpf.utils import config_from_file


class Elasticsearch(OriginalElasticsearch):
    @classmethod
    def from_config_file(cls, fp='/opt/vdsense/vdpf-configs/elastic.json', **kwargs):
        conf = config_from_file(fp)

        conf.update(kwargs)
        return cls(**conf)


class AIOElasticSearch(OriginalAIOElasticsearch, Elasticsearch):
    pass
