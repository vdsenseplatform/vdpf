from tornado.web import RequestHandler
from tornado.websocket import WebSocketHandler


class BaseHttpHandler(RequestHandler):
    def set_default_headers(self):
        # Cors headers
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS, PATCH, POST')
        self.set_header("Access-Control-Allow-Headers", "access-control-allow-origin,authorization,content-type")

        self.set_header('Content-Type', 'application/json')

    def set_status(self, status_code, reason=None):
        reasons = {
            200: 'OK', 400: 'Bad Request',
            500: 'Internal Server Error', 404: 'Not Found',
            409: 'Conflict',
            502: 'Bad Gateway'
        }
        if reason is None:
            reason = reasons.get(status_code, 'Unknown')
        return super().set_status(status_code, reason)

    def options(self):
        self.set_status(204)
        return self.finish()


class BaseWebSocketHandler(WebSocketHandler):
    def check_origin(self, str):
        return True
