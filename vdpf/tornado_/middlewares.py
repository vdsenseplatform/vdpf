import json
import functools

from vdpf.mml import Schema


def json_payload(method):
    @functools.wraps(method)
    def decorate(self, *args, **kwargs):
        body = self.request.body
        if isinstance(body, bytes):
            body = body.decode()
        
        try:
            payload = json.loads(body)
        except json.JSONDecodeError:
            self.write({
                'msg': 'Invalid JSON'
            })
            self.set_status(400)
            return self.finish()
        return method(self, *args, payload=payload, **kwargs)
    return decorate


def json_schema(schema: Schema, many=False):
    def _json_schema(method):
        @functools.wraps(method)
        @json_payload
        def decorate(self, *args, payload, **kwargs):
            parse_result = schema(strict=False).load(payload, many=many)
            if parse_result.errors:
                self.set_status(400)
                self.write({
                    'msg': 'Invalid request format',
                    'details': parse_result.errors
                })
                return self.finish()
            
            req = parse_result.data
            return method(self, *args, req=req, **kwargs)
        return decorate
    return _json_schema
